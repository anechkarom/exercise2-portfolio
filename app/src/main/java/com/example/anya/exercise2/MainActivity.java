package com.example.anya.exercise2;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText et = findViewById(R.id.et);
        Button btnSend = findViewById(R.id.btnSend);
        ImageView btnFB = findViewById(R.id.imgFB);
        ImageView btnIG = findViewById(R.id.imgIG);
        ImageView btnTG = findViewById(R.id.imgTG);

        // Принимаем edtMsg

        String msg = getIntent().getStringExtra("msg");

        // выводим принятое edtMsg
        et.setText(msg);


        btnSend.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "r0mannava@mail.ru", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Letter from Exercise2");
                //   emailIntent.putExtra(Intent.EXTRA_TEXT, msg);

                if (emailIntent.resolveActivity(getPackageManager()) == null) {
                    Toast.makeText(MainActivity.this, "No Email app found", Toast.LENGTH_SHORT).show();
                } else {
                    startActivity(Intent.createChooser(emailIntent, "Send email..."));
                }

            }
        });

        btnFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://www.facebook.com/anna.romanova.5454"));
                startActivity(i);
            }
        });
        btnIG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://www.instagram.com/r0mannava"));
                startActivity(i);
            }
        });


        btnTG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://t.me/siannarom"));
                startActivity(i);
            }
        });
    }
}
